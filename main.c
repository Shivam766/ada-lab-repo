#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100
#include "prototype.h"
#include "allfunctions.h"

char c[MAX][MAX];

int main()
{
    int i,j,m,n,k;
    char s1[MAX],s2[MAX];
    printf("Enter first string::\n");
    scanf("%s",s1);
    printf("Enter second string::\n");
    scanf("%s",s2);
    m=strlen(s1);
    n=strlen(s2);
    for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			c[i][j]=-1;
		}
	}
	
	
	printf("**********Initialized Table************\n");
	for(j=0;j<n;j++)
	{	
			printf("\t%c\    ",s2[j]);
	}
	printf("\n");
	
	
     for(i=0;i<m;i++)
	{
		printf("%c\t",s1[i]);
		for(j=0;j<n;j++)
		{	
			printf("%d\t",c[i][j]);
		}
		printf("\n");
	}
    
    k=fnLongestCommonSearchLength(s1,s2,m,n,c);
   
   
    printf("**********After Function Call************\n");
    for(j=0;j<n;j++)
	{	
			printf("\t%c\    ",s2[j]);
	}
	printf("\n");
    for(i=0;i<m;i++)
	{
		printf("%c\t",s1[i]);
		for(j=0;j<n;j++)
		{
			printf("%d\t",c[i][j]);
		}
		printf("\n");
	}
	printf("Size of Longest Common Subsequence is %d\n",k);
	fnprintlongestcommonsubsequence(s1,s2,m,n);


    return 0;
}
