int max(int a, int b)
{
    if(a>b)
        return a;
    else    
        return b;
}
int fnLongestCommonSearchLength(char s1[],char s2[],int i,int j,char c[MAX][MAX])
{
    if(i==0||j==0)
        return 0;

    if(c[i-1][j-1]!=-1)
        return c[i-1][j-1];

    if( s1[i-1]==s2[j-1])
		{
			c[i-1][j-1] = 1+fnLongestCommonSearchLength(s1,s2,i-1,j-1,c);
  			return c[i-1][j-1];
		}

    else
		{
			c[i-1][j-1] = max(fnLongestCommonSearchLength(s1,s2,i,j-1,c), fnLongestCommonSearchLength(s1,s2, i-1,j,c));
        		return c[i - 1][j - 1];
     	}

}


void fnprintlongestcommonsubsequence(char *s1, char *s2, int m, int n)
{
    int i = m;
    int j = n;
    int in = fnLongestCommonSearchLength(s1,s2,m,n,c);
    
    char str[in + 1];

    str[in]='\0';

    while (i > 0 && j > 0)
    {
        if (s1[i-1] == s2[j-1])
        {
            /* Add character to LCS string */
            str[in-1] =s1[i-1];
            in--;
            i--;
            j--;
        }
        else if (c[i-2][j-1] > c[i-1][j-2])
        {
            i--; /* going for matching character */
        }
        else
        {
            j--; /* going for matching character */
        }
    }
    printf ("Longest Common Subsequence is : %s \n",str);
}
