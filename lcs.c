/******************************************************************************
*File	: lcs.c
*Description	: Program to find longest common susequence of the two given 
*strings using recursion followed by memoization.
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100


int max(int , int );
int fnLongestCommonSearchLength(char [],char [],int ,int ,char c[][MAX]);
void fnprintlongestcommonsubsequence(char *, char *, int , int );


char c[MAX][MAX];/* Global declaration of the 2-dimensional table*/


/******************************************************************************
*Function	: max
*Input parameters: Two integers a and b
*RETURNS	: Larger number among a and b on success
******************************************************************************/

int max(int a, int b)
{
    if(a>b)
        return a;
    else    
        return b;
}


/******************************************************************************
*Function	: fnLongestCommonSearchLength
*Input parameters: 
*			char s1[]-First Input String
*			char s2[]-Second Input String
*			int  i-Size of first String
*  			int  j-Size of Second String
*			char c[MAX][MAX]-Table to store calculated entries
*RETURNS	: Last entry of the table or Length of longest common susequence
******************************************************************************/

int fnLongestCommonSearchLength(char s1[],char s2[],int i,int j,char c[MAX][MAX])
{
    if(i==0||j==0)
        return 0;

    if(c[i-1][j-1]!=-1)
        return c[i-1][j-1];

    if( s1[i-1]==s2[j-1])
		{
			c[i-1][j-1] = 1+fnLongestCommonSearchLength(s1,s2,i-1,j-1,c);
  			return c[i-1][j-1];
		}

    else
		{
			c[i-1][j-1] = max(fnLongestCommonSearchLength(s1,s2,i,j-1,c), fnLongestCommonSearchLength(s1,s2, i-1,j,c));
        		return c[i - 1][j - 1];
     	}

}


/******************************************************************************
*Function	: fnprintlongestcommonsubsequence
*Input parameters: 
*			char *s1-First Input String
*			char *s2[]-Second Input String
*			int m-Size of first String
*  			int  n-Size of Second String
*RETURNS	: Nothing
******************************************************************************/

void fnprintlongestcommonsubsequence(char *s1, char *s2, int m, int n)
{
    int i = m;
    int j = n;
    int in = fnLongestCommonSearchLength(s1,s2,m,n,c);
    
    char str[in + 1];

    str[in]='\0';

    while (i > 0 && j > 0)
    {
        if (s1[i-1] == s2[j-1])
        {
            /* Add character to LCS string */
            str[in-1] =s1[i-1];
            in--;
            i--;
            j--;
        }
        else if (c[i-2][j-1] > c[i-1][j-2])
        {
            i--; /* going for matching character */
        }
        else
        {
            j--; /* going for matching character */
        }
    }
    printf ("Longest Common Subsequence is : %s \n",str);
}


/******************************************************************************
*Function	: main
*Input parameters: no parameters
*RETURNS	: 0 on success
******************************************************************************/

int main()
{
    int i,j,m,n,k;
    char s1[MAX],s2[MAX];
    printf("Enter first string::\n");
    scanf("%s",s1);
    printf("Enter second string::\n");
    scanf("%s",s2);
    m=strlen(s1);
    n=strlen(s2);
    for(i=0;i<m;i++)
	{
		for(j=0;j<n;j++)
		{
			c[i][j]=-1;
		}
	}
	
	
	printf("**********Initialized Table************\n");
	for(j=0;j<n;j++)
	{	
			printf("\t%c\    ",s2[j]);
	}
	printf("\n");
	
	
     for(i=0;i<m;i++)
	{
		printf("%c\t",s1[i]);
		for(j=0;j<n;j++)
		{	
			printf("%d\t",c[i][j]);
		}
		printf("\n");
	}
    
    k=fnLongestCommonSearchLength(s1,s2,m,n,c);
   
   
    printf("**********After Function Call************\n");
    for(j=0;j<n;j++)
	{	
			printf("\t%c\    ",s2[j]);
	}
	printf("\n");
    for(i=0;i<m;i++)
	{
		printf("%c\t",s1[i]);
		for(j=0;j<n;j++)
		{
			printf("%d\t",c[i][j]);
		}
		printf("\n");
	}
	printf("Size of Longest Common Subsequence is %d\n",k);
	fnprintlongestcommonsubsequence(s1,s2,m,n);


    return 0;
}
