Problem No 4. Given two strings find the Longest Common Subsequence. For example, if X is
CTGCTGA and Y is TGTGT, then the longest subsequence is TGTG. Design and
implement the solution to the above problem.